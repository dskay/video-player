# Video Search Player

This web application utilizes the YouTube API in order to search for videos and compiles them in one area for ease of access.

## Use
In order to access the web application, you can either clone the application and run it locally by running the following commands:
* `yarn install` 
* `yarn start`

You can also visit a live website hosted on [Heroku](https://www.heroku.com).

Click here to visit the live website: [Video Player](https://video-player-dkim.herokuapp.com/)

## Demo
Click on the image below in order to view the live website.

[![demo](/screenshots/video-player.png)](https://video-player-dkim.herokuapp.com/)

## Purpose
Purpose of this application is to familiarize myself with the YouTube API.
