import React, { Component } from "react";
import ReactDOM from "react-dom";
import YTSearch from "youtube-api-search";
import _ from "lodash";

import SearchBar from "./components/SearchBar";
import VideoList from "./components/VideoList";
import VideoDetail from "./components/VideoDetail";

const API_KEY = "AIzaSyDeBJq9WUypIc3y732sbnyPBR77Gm-tm0Q";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      videos: [],
      selectedVideo: null,
      searchTerm: ""
    };

    this.videoSearch("surfboards");

    this.onVideoSelect = this.onVideoSelect.bind(this);
  }

  onVideoSelect(selectedVideo) {
    this.setState({
      selectedVideo
    });
  }

  videoSearch(term) {
    // searches YouTube
    YTSearch({ key: API_KEY, term }, videos => {
      this.setState({
        videos,
        selectedVideo: videos[0]
      });
    });
  }

  render() {
    const throttleTime = 700;

    // throttle the search
    const videoSearch = _.debounce(term => {
      this.videoSearch(term);
    }, throttleTime);

    return (
      <div>
        <SearchBar onSearchTermChange={videoSearch} />
        <VideoDetail video={this.state.selectedVideo} />
        <VideoList videos={this.state.videos} onVideoSelect={this.onVideoSelect} />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.querySelector(".container"));
